from flask import Flask, render_template
import RPi.GPIO as GPIO
import time

app = Flask(__name__)

# Define GPIO pins
moisture_pin = 29
relay_pin = 37

# Set up GPIO mode and pin configuration
GPIO.setmode(GPIO.BOARD)
GPIO.setup(moisture_pin, GPIO.IN)
GPIO.setup(relay_pin, GPIO.OUT)

def get_moisture_status():
    moisture_level = GPIO.input(moisture_pin)
    if moisture_level == 0:
        return "Wet"
    else:
        return "Dry"

@app.route('/')
def index():
    moisture_status = get_moisture_status()
    return render_template('index.html', moisture_status=moisture_status)

@app.route('/water')
def water_plants():
    # Activate solenoid valve to water the plants
    GPIO.output(relay_pin, GPIO.HIGH)
    time.sleep(60)  # Watering duration: 1 minute
    GPIO.output(relay_pin, GPIO.LOW)
    return 'Plants watered successfully!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
